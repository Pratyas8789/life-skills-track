pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop$ git clone https://gitexercises.fracz.com/git/exercises.git
cd exercises
git config user.name "Pratyas kumar"
git config user.email "pratyas.kumar.23.2@mountblue.tech"
./configure.sh
git start
Cloning into 'exercises'...
remote: Counting objects: 150, done.
remote: Compressing objects: 100% (114/114), done.
remote: Total 150 (delta 33), reused 149 (delta 33)
Receiving objects: 100% (150/150), 34.95 KiB | 1022.00 KiB/s, done.
Resolving deltas: 100% (33/33), done.
OK
Preparing the exercise environment, hold on...
Exercise master started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/master
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start
Preparing the exercise environment, hold on...
Exercise master started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/master
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the master exercise. Hold on...
Exercise: master        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/master/xuye        

Next task: commit-one-file        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add .
(for add all the file we use git add .)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
(to find status)
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start commit-one-file
Preparing the exercise environment, hold on...
Exercise commit-one-file started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/commit-one-file
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch commit-one-file
Your branch is up to date with 'origin/commit-one-file'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	A.txt
	B.txt

nothing added to commit but untracked files present (use "git add" to track)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
A.txt  B.txt  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add A.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "Some commit message"
()
[commit-one-file 69a596d] Some commit message
 1 file changed, 1 insertion(+)
 create mode 100644 A.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the commit-one-file exercise. Hold on...
Exercise: commit-one-file        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/commit-one-file/xuye        

Next task: commit-one-file-staged        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start commit-one-file-staged
Preparing the exercise environment, hold on...
Exercise commit-one-file-staged started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/commit-one-file-staged
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
A.txt  B.txt  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch commit-one-file-staged
Your branch is up to date with 'origin/commit-one-file-staged'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   A.txt
	new file:   B.txt

pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git reset 
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch commit-one-file-staged
Your branch is up to date with 'origin/commit-one-file-staged'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	A.txt
	B.txt

nothing added to commit but untracked files present (use "git add" to track)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add A.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch commit-one-file-staged
Your branch is up to date with 'origin/commit-one-file-staged'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   A.txt

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	B.txt

pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "a"
[commit-one-file-staged 53d0423] a
 1 file changed, 1 insertion(+)
 create mode 100644 A.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the commit-one-file-staged exercise. Hold on...
Exercise: commit-one-file-staged        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/commit-one-file-staged/xuye        

Next task: ignore-them        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start ignore-them
Preparing the exercise environment, hold on...
Exercise ignore-them started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/ignore-them
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch ignore-them
Your branch is up to date with 'origin/ignore-them'.

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	file.txt
	libraries/
	output.o
	program.exe

nothing added to commit but untracked files present (use "git add" to track)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
(to find all the data we use ls command)
file.txt  libraries  output.o  program.exe  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ touch .gitignore
(to make a file we use touch command)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
file.txt  libraries  output.o  program.exe  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano .gitignore
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add .gitignore
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch ignore-them
Your branch is up to date with 'origin/ignore-them'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	file.txt
	program.exe

pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano .gitignore
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add .gitignore
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch ignore-them
Your branch is up to date with 'origin/ignore-them'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   .gitignore

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	file.txt

pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "added new file"
[ignore-them 675fdd4] added new file
 1 file changed, 4 insertions(+)
 create mode 100644 .gitignore
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the ignore-them exercise. Hold on...
Exercise: ignore-them        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/ignore-them/xuye        

Next task: chase-branch        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start chase-branch
Preparing the exercise environment, hold on...
Exercise chase-branch started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/chase-branch
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge
Already up to date.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge escaped
Updating 0d3aec7..b97e2d6
Fast-forward
 file.txt | 2 ++
 1 file changed, 2 insertions(+)
 create mode 100644 file.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the chase-branch exercise. Hold on...
Exercise: chase-branch        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/chase-branch/xuye        

Next task: merge-conflict        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start merge-conflict
Preparing the exercise environment, hold on...
Exercise merge-conflict started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/merge-conflict
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git branch
  another-piece-of-work
  chase-branch
  commit-one-file
  commit-one-file-staged
  escaped
  ignore-them
  master
* merge-conflict
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge another-piece-of-work
Auto-merging equation.txt
CONFLICT (content): Merge conflict in equation.txt
Automatic merge failed; fix conflicts and then commit the result.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
equation.txt  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch merge-conflict
Your branch is ahead of 'origin/merge-conflict' by 2 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   equation.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano file.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch merge-conflict
Your branch is ahead of 'origin/merge-conflict' by 2 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   equation.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "mycommit"
[merge-conflict 2a0a8c0] mycommit
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the merge-conflict exercise. Hold on...
Exercise: merge-conflict        
Status: FAILED        
You didn't resolve the conflict properly (expected 2+3=5 equation).        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start merge-conflict
Preparing the exercise environment, hold on...
Exercise merge-conflict started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/merge-conflict
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge another-piece-of-work
Auto-merging equation.txt
CONFLICT (content): Merge conflict in equation.txt
Automatic merge failed; fix conflicts and then commit the result.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch merge-conflict
Your branch is ahead of 'origin/merge-conflict' by 2 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   equation.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "mycommit"
[merge-conflict 9e01de2] mycommit
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the merge-conflict exercise. Hold on...
Exercise: merge-conflict        
Status: FAILED        
You didn't resolve the conflict properly (expected 2+3=5 equation).        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start merge-conflict
Preparing the exercise environment, hold on...
Exercise merge-conflict started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/merge-conflict
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "mycommt"
On branch merge-conflict
Your branch is ahead of 'origin/merge-conflict' by 2 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   equation.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "mycommt"
[merge-conflict bb79ddd] mycommt
 1 file changed, 1 insertion(+), 1 deletion(-)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the merge-conflict exercise. Hold on...
Exercise: merge-conflict        
Status: FAILED        
Expected number of commits: 4. Received 3.        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start merge-conflict
Preparing the exercise environment, hold on...
Exercise merge-conflict started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/merge-conflict
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge another-piece-of-work
Auto-merging equation.txt
CONFLICT (content): Merge conflict in equation.txt
Automatic merge failed; fix conflicts and then commit the result.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch merge-conflict
Your branch is ahead of 'origin/merge-conflict' by 2 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   equation.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "mycommt"
[merge-conflict 1ac5eb9] mycommt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "mycomt"
On branch merge-conflict
Your branch is ahead of 'origin/merge-conflict' by 4 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the merge-conflict exercise. Hold on...
Exercise: merge-conflict        
Status: FAILED        
You didn't resolve the conflict properly (expected 2+3=5 equation).        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start merge-conflict
Preparing the exercise environment, hold on...
Exercise merge-conflict started!
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge another-piece-of-work
Auto-merging equation.txt
CONFLICT (content): Merge conflict in equation.txt
Automatic merge failed; fix conflicts and then commit the result.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch merge-conflict
Your branch is ahead of 'origin/merge-conflict' by 2 commits.
  (use "git push" to publish your local commits)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   equation.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "1"
[merge-conflict bf7df53] 1
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the merge-conflict exercise. Hold on...
Exercise: merge-conflict        
Status: FAILED        
You didn't resolve the conflict properly (expected 2+3=5 equation).        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start merge-conflict
Preparing the exercise environment, hold on...
Exercise merge-conflict started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/merge-conflict
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge another-piece-of-work
Auto-merging equation.txt
CONFLICT (content): Merge conflict in equation.txt
Automatic merge failed; fix conflicts and then commit the result.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git branch
  another-piece-of-work
  chase-branch
  commit-one-file
  commit-one-file-staged
  escaped
  ignore-them
  master
* merge-conflict
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge another-piece-of-work
fatal: You have not concluded your merge (MERGE_HEAD exists).
Please, commit your changes before you merge.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "11"
[merge-conflict 54f157f] 11
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git merge another-piece-of-work
Already up to date.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the merge-conflict exercise. Hold on...
Exercise: merge-conflict        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/merge-conflict/xuye        

Next task: save-your-work        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start save-your-work
Preparing the exercise environment, hold on...
Exercise save-your-work started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/save-your-work
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nono equation.txt
Command 'nono' not found, did you mean:
  command 'nano' from snap nano (5.7+pkg-4057)
  command 'nino' from snap nino (1.3.1)
  command 'nodo' from snap nodo (master)
  command 'nano' from deb nano (6.2-1)
  command 'mono' from deb mono-runtime (6.8.0.105+dfsg-3.2)
See 'snap info <snapname>' for additional versions.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano equation.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start save-your-work
Preparing the exercise environment, hold on...
Exercise save-your-work started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/save-your-work
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch save-your-work
Your branch is ahead of 'origin/save-your-work' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   bug.txt
	modified:   program.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git stash
Saved working directory and index state WIP on save-your-work: af828df Excellent version with a bug
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch save-your-work
Your branch is ahead of 'origin/save-your-work' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano bug.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git stash
Saved working directory and index state WIP on save-your-work: af828df Excellent version with a bug
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git stash pop
On branch save-your-work
Your branch is ahead of 'origin/save-your-work' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   bug.txt

no changes added to commit (use "git add" and/or "git commit -a")
Dropped refs/stash@{0} (958fbadae2f91d3ec7f2f3046d7f9b71e7e91649)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "1"
On branch save-your-work
Your branch is ahead of 'origin/save-your-work' by 1 commit.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   bug.txt

no changes added to commit (use "git add" and/or "git commit -a")
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano bug.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add bug.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "111"
[save-your-work 294c160] 111
 1 file changed, 1 deletion(-)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git stash pop
Auto-merging bug.txt
On branch save-your-work
Your branch is ahead of 'origin/save-your-work' by 2 commits.
  (use "git push" to publish your local commits)

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   bug.txt
	modified:   program.txt

no changes added to commit (use "git add" and/or "git commit -a")
Dropped refs/stash@{0} (c27b3755bd79a9c9519ed6114a053839eb3d416d)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano program.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano bug.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add bug.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add program.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "2"
[save-your-work e70a1c5] 2
 2 files changed, 7 insertions(+), 2 deletions(-)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the save-your-work exercise. Hold on...
Exercise: save-your-work        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/save-your-work/xuye        

Next task: change-branch-history        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start change-branch-history
Preparing the exercise environment, hold on...
Exercise change-branch-history started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/change-branch-history
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start change-branch-history
Preparing the exercise environment, hold on...
Exercise change-branch-history started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/change-branch-history
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch change-branch-history
Your branch is ahead of 'origin/change-branch-history' by 2 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git rebase hot-bugfix
Successfully rebased and updated refs/heads/change-branch-history.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the change-branch-history exercise. Hold on...
Exercise: change-branch-history        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/change-branch-history/xuye        

Next task: remove-ignored        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start remove-ignored
Preparing the exercise environment, hold on...
Exercise remove-ignored started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/remove-ignored
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git status
On branch remove-ignored
Your branch is ahead of 'origin/remove-ignored' by 2 commits.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
ignored.txt  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git rm ignored.txt
rm 'ignored.txt'
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "2"
[remove-ignored 96b86db] 2
 1 file changed, 1 deletion(-)
 delete mode 100644 ignored.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the remove-ignored exercise. Hold on...
Exercise: remove-ignored        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/remove-ignored/xuye        

Next task: case-sensitive-filename        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start case-sensitive-filename
Preparing the exercise environment, hold on...
Exercise case-sensitive-filename started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/case-sensitive-filename
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
File.txt  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git mv File.txt file.txt
(for rename)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add file.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "2"
[case-sensitive-filename d70cb0d] 2
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename File.txt => file.txt (100%)
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the case-sensitive-filename exercise. Hold on...
Exercise: case-sensitive-filename        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/case-sensitive-filename/xuye        

Next task: fix-typo        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        

pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start fix-typo
Preparing the exercise environment, hold on...
Exercise fix-typo started!
Read the README.md for instructions or view them in browser:
http://gitexercises.fracz.com/e/fix-typo
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ ls
file.txt  README.md  start.sh
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ nano file.txt 
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git start fix-typo
Preparing the exercise environment, hold on...
Exercise fix-typo started!
Read the README.md for instructions or view them in browser:
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -a --amend
(to change the commit name)
[fix-typo 834b0c0] Add Hello world
 Date: Mon Feb 13 17:02:12 2023 +0530
 1 file changed, 1 insertion(+)
 create mode 100644 file.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the fix-typo exercise. Hold on...
Exercise: fix-typo        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/fix-typo/xuye        

Next task: forge-date        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye        
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ 

pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit --amend --no-edit --date="1987-08-03"
[forge-date 0988abf] Late work
 Date: Mon Aug 3 17:14:44 1987 +0530
 1 file changed, 1 insertion(+)
 create mode 100644 work.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ get verify
Command 'get' not found, but there are 18 similar ones.
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the forge-date exercise. Hold on...
Exercise: forge-date        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/forge-date/xuye        

Next task: fix-old-typo        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye      

pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git reset HEAD^
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add first.txt 
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "First.txt"
[split-commit b8d84ea] First.txt
 1 file changed, 1 insertion(+)
 create mode 100644 first.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git add second.txt 
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git commit -m "Second.txt"
[split-commit 7385527] Second.txt
 1 file changed, 1 insertion(+)
 create mode 100644 second.txt
pratyas@pratyas-ZenBook-UX325EA-UX325EA:~/Desktop/exercises$ git verify
Verifying the split-commit exercise. Hold on...
Exercise: split-commit        
Status: PASSED        
You can see the easiest known solution and further info at:        
https://gitexercises.fracz.com/e/split-commit/xuye        

Next task: fix-old-typo        
In order to start, execute: git start next        

See your progress and instructions at:        
https://gitexercises.fracz.com/c/xuye 
